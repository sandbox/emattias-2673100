<?php

$plugin = array(
  'title' => 'Google Custom Search API integration',
  'context' => 'google_cse_api_context_create_google_cse_api',
  'defaults' => array(),
  'keyword' => 'google_cse_api',
  'context name' => 'google_cse_api',
);

function google_cse_api_context_create_google_cse_api($empty, $search_query_context = NULL, $conf = FALSE) {
  $google_cse_api_context = new ctools_context('google_cse_api');
  $google_cse_api_context->plugin = 'google_cse_api';

  if ($empty || empty($_GET['search'])) {
    return $google_cse_api_context;
  }

  $response = google_cse_api_search($_GET['search']);

  if(!empty($response->data) && is_string($response->data)){
    $response->data = json_decode($response->data);
  }

  $google_cse_api_context->data = $response->data;

  return $google_cse_api_context;
}
