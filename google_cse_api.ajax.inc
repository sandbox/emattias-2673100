<?php

function google_cse_api_ajax_search($query) {
  $search_result = google_cse_api_search($query);
  return $search_result->data;
}

function google_cse_api_ajax_search_deliver($page_callback_result){
  drupal_add_http_header('Content-Type', 'application/json');
  print $page_callback_result;
  ajax_footer();
}