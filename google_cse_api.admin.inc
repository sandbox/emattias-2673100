<?php

/**
 * Settings form.
 */
function google_cse_api_settings_form($form, &$form_state) {

  $form['google_cse_api_cx'] = array(
    '#type' => 'textfield',
    '#title' => t('CX'),
    '#description' => google_cse_api_api_keys_description(),
    '#default_value' => variable_get('google_cse_api_cx', ''),
  );

  $form['google_cse_api_apikey'] = array(
    '#type' => 'textfield',
    '#title' => t('API key'),
    '#default_value' => variable_get('google_cse_api_apikey', ''),
    '#description' => google_cse_api_api_keys_description(),
  );

  $form['google_cse_api_items_per_page'] = array(
    '#type' => 'textfield',
    '#title' => t('Items per page'),
    '#default_value' => variable_get('google_cse_api_items_per_page', '10'),
    '#description' => t('Maximum of 10')
  );

  $vocabulary_options = array();
  foreach (taxonomy_get_vocabularies() as $vid => $vocabulary) {
    $vocabulary_options[$vid] = $vocabulary->name;
  }
  $form['google_cse_api_taxonomy_vocabulary'] = array(
    '#type' => 'checkboxes',
    '#title' => t('Choose the vocabulary to be referenced by google'),
    '#options' => $vocabulary_options,
    '#default_value' => variable_get('google_cse_api_taxonomy_vocabulary', array()),
  );

  return system_settings_form($form);
}

function google_cse_api_api_keys_description(){
  return t('You can find it at !link', array('!link' => l('https://cse.google.com', 'https://cse.google.com', array('attributes' => array('target' => '_blank')))));
}