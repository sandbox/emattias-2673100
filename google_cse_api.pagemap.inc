<?php
/**
 * @file
 * Functions for better google search index.
 */
/**
 * Implements hook_init().
 *
 * Google CSE support recommended that we add the PageMap data to the HTML the
 * first time that the pages are indexed with PageMap data, then only sitemap
 * is enough.
 */
function google_cse_api_init() {
  if (arg(0) == 'node' & is_numeric(arg(1)) && variable_get('google_cse_api_integration_add_page_map_to_html', 1)) {
    if ($node = node_load(arg(1))) {
      $page_map_xml = format_xml_elements(array(google_cse_api_integration_build_page_map_renderable(arg(1), FALSE)));
      $page_map_renderable = array(
        '#type' => 'markup',
        '#markup' => '<!--' . "\n" . $page_map_xml . '-->' . "\n",
      );
      drupal_add_html_head($page_map_renderable, 'google_cse_api_pagemap');
    }
  }
}
/**
 * Implements hook_xmlsitemap_element_alter().
 *
 * Adds attributes.
 */
function google_cse_api_integration_xmlsitemap_element_alter(array &$element, array $link) {
  if ($link['type'] == 'node') {
    $element[] = google_cse_api_integration_build_page_map_renderable($link['id']);
  }
}
/**
 * Looping though entities and adding the needed values for search.
 */
function google_cse_api_integration_build_page_map_renderable($nid, $add_xmlns = TRUE) {
  $node_wrapper = entity_metadata_wrapper('node', node_load($nid));
  // Adding bundle.
  $data_object_children = array(
    array(
      'key' => 'Attribute',
      'attributes' => array(
        'name' => 'bundle',
      ),
      'value' => $node_wrapper->getBundle(),
    ),
    array(
      'key' => 'Attribute',
      'attributes' => array(
        'name' => 'id',
      ),
      'value' => $node_wrapper->getIdentifier(),
    ),
  );
  // Get the selected taxonomy vocabulary from the configuration.
  $vocabularies = array_filter(variable_get('google_cse_taxonomy_vocabulary', array()));

  // @TODO: Avoid doing DB query when not needed!

  // Get the all the term and vocabulary of the node.
  $query = db_select('taxonomy_term_data', 'ttd');
  $query->fields('ttd', array('vid'));
  $query->join('taxonomy_index', 'ti', 'ttd.tid = ti.tid');
  $query ->fields('ti', array('tid'));
  $query->join('taxonomy_vocabulary', 'tv', 'ttd.vid = tv.vid');
  $query ->fields('tv', array('machine_name'));
  $query->condition('ti.nid', $node_wrapper->getIdentifier());
  $query->condition('tv.vid', $vocabularies);

  $results = $query
    ->distinct()
    ->execute();

  if($results){
    // Adding taxonomy to the map.
    foreach ($results as $result) {
      $data_object_children[] = array(
        'key' => 'Attribute',
        'attributes' => array(
          'name' => $result->machine_name,
        ),
        'value' => $result->tid,
      );
    }
  }

  if (isset($data_object_children)) {
    $page_map = array(
      'key' => 'PageMap',
      'value' => array(
        array(
          'key' => 'DataObject',
          'attributes' => array(
            'type' => 'node',
          ),
          'value' => $data_object_children,
        ),
      ),
    );
  }

  if (!empty($add_xmlns)) {
    $page_map['attributes']['xmlns'] = 'http://www.google.com/schemas/sitemap-pagemap/1.0';
  }

  if (isset($page_map)) {
    return $page_map;
  }
}
